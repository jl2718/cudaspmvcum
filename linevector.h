// Our special 'read a line as a vector' object
template <typename T>
struct linevector_t: public std::vector <T> { };

// Cast function (mutable object)
template <typename T>
linevector_t <T> &
linevector( std::vector <T> & v ){
  return static_cast <linevector_t <T> &> ( v );
}

// Cast functions (const object)
template <typename T>
const linevector_t <T> &
linevector( const std::vector <T> & v ){
  return static_cast <const linevector_t <T> &> ( v );
}

// Input operation to read a single line into a vector <T>
template <typename T>
std::istream& operator >> ( std::istream& ins, linevector_t <T> & v ){
  std::string s;
  std::getline( ins, s );
  std::istringstream iss( s );
  v.clear();
  std::copy( std::istream_iterator <T> ( iss ), std::istream_iterator <T> (), std::back_inserter( v ) );
  return ins;
}

// Something useful for our example program
template <typename T>
std::ostream& operator << ( std::ostream& outs, const linevector_t <T> & v ){
  std::copy( v.begin(), v.end() - 1, std::ostream_iterator <T> ( outs, " " ) );
  return outs << v.back() << std::endl;
}
