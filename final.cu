#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <numeric>
#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <thrust/remove.h>
#include <thrust/random.h>
#include <thrust/random/uniform_int_distribution.h>
#include <thrust/for_each.h>
#include <thrust/scan.h>
#include <thrust/sort.h>
#include <thrust/adjacent_difference.h>
#include "omp.h"

#include "linevector.h"
#include "mp1-util.h"

#define uint unsigned int

struct printf_functor{
  __host__ __device__
  void operator()(unsigned int x){
    printf("%d ",x);
  }
  __host__ __device__
  void operator()(unsigned char x){
    printf("%c",x);
  }
  __host__ __device__
  void operator()(float x){
    printf("%f ",x);
  }
};
/*
float calc_serial(std::vector<float> &A,std::vector<uint> &K,std::vector<uint> &S,std::vector<float> &X);
float calc_omp(std::vector<float> &A,std::vector<uint> &K,std::vector<uint> &S,std::vector<float> &X);
float calc_thrust0(std::vector<float> &A,std::vector<uint> &K,std::vector<uint> &S,std::vector<float> &X);
float calc_thrust1(std::vector<float> &A,std::vector<uint> &K,std::vector<uint> &S,std::vector<float> &X);
float calc_thrust2(std::vector<float> &A,std::vector<uint> &K,std::vector<uint> &S,std::vector<float> &X);
float calc_thrust3(std::vector<float> &A,std::vector<uint> &K,std::vector<uint> &S,std::vector<float> &X);
float calc_csr(std::vector<float> &A,std::vector<uint> &K,std::vector<uint> &S,std::vector<float> &X);
float calc_csr_ordered(std::vector<float> &A,std::vector<uint> &K,std::vector<uint> &S,std::vector<float> &X);
float calc_csr_multi(std::vector<float> &A,std::vector<uint> &K,std::vector<uint> &S,std::vector<float> &X);
*/

void check(std::vector<float> A,std::vector<float> B);

#define tvec thrust::device_vector// host for easy debugging
#define hvec thrust::host_vector
#define dvec thrust::device_vector

//globals (sorry, I'm lazy)
uint n,p,q,N;


#include "calc_serial.inc"
#include "calc_thrust.inc"
#include "calc_csr.inc"

struct op_abs : public std::unary_function<float,float>{
  float operator()(float x){return std::abs(x);}
};
struct op_sqr : public std::unary_function<float,float>{
  float operator()(float x){return x*x;}
};
void check(std::vector<float> A,std::vector<float> B){
  float e;
  std::vector<float> D(n),C(n),E(n);
  std::transform(A.begin(),A.end(),B.begin(),D.begin(),std::minus<float>());
  std::transform(D.begin(),D.end(),E.begin(),op_abs());
  std::transform(A.begin(),A.end(),C.begin(),op_abs());
  e =  *std::max_element(E.begin(),E.end()) / *std::max_element(C.begin(),C.end());
  std::cout << "LInf relative Error: " << e << std::endl;
  e =  std::accumulate(E.begin(),E.end(),0)/std::accumulate(C.begin(),C.end(),0);
  std::cout << "L1 relative Error: " << e << std::endl;
  std::transform(D.begin(),D.end(),E.begin(),op_sqr());
  std::transform(A.begin(),A.end(),C.begin(),op_sqr());
  e =  std::sqrt(std::accumulate(E.begin(),E.end(),0))/std::sqrt(std::accumulate(C.begin(),C.end(),0));
  std::cout << "L2 relative Error: " << e << std::endl;
}

int main(int argc, char **argv){
  int nd;
  cudaGetDeviceCount(&nd);
  printf("%d CUDA devices available\n",nd);
  char* sfa = "a.txt";
  if(argc>1) sfa = argv[1];
  char* sfx = "x.txt";
  if(argc>2) sfx = argv[2];
  char* sfb = "b.txt";
  if(argc>3) sfb = argv[3];
  std::ifstream ifs(sfa);
  if(!ifs.good()) return 1;
  std::string sline;
  std::istringstream iss;
  std::getline(ifs,sline);
  iss.str(sline);
  iss >> n >> p >> q >> N;
  std::vector<float> A(n),B(n),C(n),X(q),D(n);
  std::vector<uint> K(n),S(p);
  ifs >> linevector(A);
  ifs >> linevector(S);
  ifs >> linevector(K);
  printf("n:%d p:%d q:%d N:%d\n",n,p,q,N);
  //std::for_each(A.begin(),A.begin()+10,printf_functor());
  ifs.close();
  ifs.open(sfx);
  if(!ifs.good()) return 1;
  ifs >> linevector(X);
  ifs.close();
  float t;
  C=A;calc_serial(C,K,S,X);
  B=A;calc_omp(B,K,S,X);
  B=A;calc_thrust0(B,K,S,X);
  B=A;calc_thrust1(B,K,S,X);
  D=A;t=calc_thrust2(D,K,S,X);
  B=A;calc_thrust3(B,K,S,X);
  B=A;calc_csr_ordered(B,K,S,X);
  B=A;calc_blockcsr(B,K,S,X);
  B=A;calc_csr(B,K,S,X);
  B=A;calc_csr_multi(B,K,S,X);
  B=A;calc_aligned_multi(B,K,S,X);
  //check(C,D);//uncomment this line and the calc_serial line to check error
  //printf("The running time of my code for %d iterations is: %05.0f milliseconds\n",N,t);
  std::ofstream ofs(sfb,std::ios::binary);
  if(!ofs.good()) return 1;
  ofs << linevector(D);
  ofs.close();
  /**/
}
