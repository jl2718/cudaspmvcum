
float calc_serial(std::vector<float> &A,std::vector<uint> &K,std::vector<uint> &S,std::vector<float> &X){
  omp_set_num_threads(64);
  event_pair timer;
  start_timer(&timer);
  for(int i=0;i<N;i++){
    for(int r=1;r<p;r++){
      float sum =0;
      for(int j=S[r-1];j<S[r];j++){
	sum+=A[j]*X[K[j]];
	A[j]=sum;
      }
    }
  }
  return stop_timer(&timer,"Serial");
}

float calc_omp(std::vector<float> &A,std::vector<uint> &K,std::vector<uint> &S,std::vector<float> &X){
  omp_set_num_threads(64);
  event_pair timer;
  start_timer(&timer);
  for(int i=0;i<N;i++){
    #pragma omp parallel for
    for(int r=1;r<p;r++){
      float sum =0;
      for(int j=S[r-1];j<S[r];j++){
	sum+=A[j]*X[K[j]];
	A[j]=sum;
      }
    }
  }
  return stop_timer(&timer,"OpenMP");
}

