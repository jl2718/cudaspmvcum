
float calc_thrust0(std::vector<float> &A,std::vector<uint> &K,std::vector<uint> &S,std::vector<float> &X){
  tvec<float> tX(X);
  tvec<float> tA(A);
  tvec<uint> tK(K);
  tvec<uint> tS(S);
  tvec<float> tZ(n);
  tvec<uint> tG(n,0);
  
  thrust::fill
    (thrust::permutation_iterator<tvec<uint>::iterator,tvec<uint>::iterator>(tG.begin(), tS.begin()),
     thrust::permutation_iterator<tvec<uint>::iterator,tvec<uint>::iterator>(tG.begin(), tS.end()),
     1);
  thrust::inclusive_scan(tG.begin(),tG.end(),tG.begin());
  
  event_pair timer;
  start_timer(&timer);
  for(int i=0;i<N;i++){
    thrust::transform
      (tA.begin(),tA.end(),
       thrust::permutation_iterator<tvec<float>::iterator,tvec<uint>::iterator>(tX.begin(), tK.begin()),
       tZ.begin(),thrust::multiplies<float>());
    thrust::inclusive_scan_by_key(tG.begin(),tG.end(),tZ.begin(),tA.begin());
  }

  check_launch("Thrust0");
  float t=stop_timer(&timer,"Thrust0");
  thrust::copy(tA.begin(),tA.end(),A.begin());
  return t;
}


float calc_thrust1(std::vector<float> &A,std::vector<uint> &K,std::vector<uint> &S,std::vector<float> &X){
  tvec<float> tX(X);
  tvec<float> tA(A);
  tvec<uint> tK(K);
  tvec<uint> tS(S);
  tvec<float> tZ(n);
  tvec<uint> tG(n,0);
  tvec<uint> tC(n);
  
  //create segmented scan vector G
  thrust::fill
    (thrust::permutation_iterator<tvec<uint>::iterator,tvec<uint>::iterator>(tG.begin(), tS.begin()),
     thrust::permutation_iterator<tvec<uint>::iterator,tvec<uint>::iterator>(tG.begin(), tS.end()),
     1);
  thrust::inclusive_scan(tG.begin(),tG.end(),tG.begin());  
  
  //order K by reference into X for efficient memory access, store order in C
  thrust::copy_n(thrust::counting_iterator<uint>(0),n,tC.begin());
  thrust::sort_by_key(tK.begin(),tK.end(),tC.begin());  
  
  event_pair timer;
  start_timer(&timer);
  for(int i=0;i<N;i++){
    thrust::transform
      (thrust::permutation_iterator<tvec<float>::iterator,tvec<uint>::iterator>(tA.begin(), tC.begin()),
       thrust::permutation_iterator<tvec<float>::iterator,tvec<uint>::iterator>(tA.begin(), tC.end()),
       thrust::permutation_iterator<tvec<float>::iterator,tvec<uint>::iterator>(tX.begin(), tK.begin()),
       thrust::permutation_iterator<tvec<float>::iterator,tvec<uint>::iterator>(tZ.begin(), tC.begin()),
       thrust::multiplies<float>());
    thrust::inclusive_scan_by_key(tG.begin(),tG.end(),tZ.begin(),tA.begin());
  }
  check_launch("Thrust1");
  float t=stop_timer(&timer,"Thrust1");
  thrust::copy(tA.begin(),tA.end(),A.begin());
  return t;
}

float calc_thrust2(std::vector<float> &A,std::vector<uint> &K,std::vector<uint> &S,std::vector<float> &X){
  tvec<float> tX(X);
  tvec<float> tA(A);
  tvec<uint> tK(K);
  tvec<uint> tS(S);
  tvec<float> tY(n);
  //tvec<float> tZ(n);
  tvec<uint> tG(n,0);
  
  thrust::fill(thrust::permutation_iterator<tvec<uint>::iterator,tvec<uint>::iterator>(tG.begin(), tS.begin()),
	       thrust::permutation_iterator<tvec<uint>::iterator,tvec<uint>::iterator>(tG.begin(), tS.end()),
	       1);
  thrust::inclusive_scan(tG.begin(),tG.end(),tG.begin());
  
  thrust::copy(thrust::permutation_iterator<tvec<float>::iterator,tvec<uint>::iterator>(tX.begin(), tK.begin()),
	       thrust::permutation_iterator<tvec<float>::iterator,tvec<uint>::iterator>(tX.begin(), tK.end()),
	       tY.begin());
  /*
  thrust::for_each(tA.begin(),tA.end(),printf_functor());printf("\n");
  thrust::for_each(tY.begin(),tY.end(),printf_functor());printf("\n");
  thrust::for_each(tG.begin(),tG.end(),printf_functor());printf("\n");
  */
  event_pair timer;
  start_timer(&timer);
  for(int i=0;i<N;i++){
    thrust::transform(tA.begin(),tA.end(),
		      tY.begin(),
		      tA.begin(),thrust::multiplies<float>());
    thrust::inclusive_scan_by_key(tG.begin(),tG.end(),tA.begin(),tA.begin());
  }
  check_launch("Thrust2");
  float t=stop_timer(&timer,"Thrust2");
  thrust::copy(tA.begin(),tA.end(),A.begin());
  return t;
}


float calc_thrust3(std::vector<float> &A,std::vector<uint> &K,std::vector<uint> &S,std::vector<float> &X){
  int nd;
  cudaGetDeviceCount(&nd);
  omp_set_num_threads(nd);
  //printf("%d CUDA devices available\n",nd);

  uint q[nd+1];
  hvec<float>  Y(n);
  hvec<uint>   G(n,0);
  hvec<uint>  tS(S);
  hvec<float> tX(X);
  hvec<uint>  tK(K);
  thrust::device_ptr<float> dpA[nd];
  thrust::device_ptr<float> dpY[nd];
  thrust::device_ptr<uint>  dpG[nd];
  
  thrust::fill(thrust::permutation_iterator<hvec<uint>::iterator,hvec<uint>::iterator>(G.begin(), tS.begin()),
	       thrust::permutation_iterator<hvec<uint>::iterator,hvec<uint>::iterator>(G.begin(), tS.end()),
	       1);
  
  thrust::inclusive_scan(G.begin(),G.end(),G.begin());
    
  thrust::copy(thrust::permutation_iterator<hvec<float>::iterator,hvec<uint>::iterator>(tX.begin(), tK.begin()),
	       thrust::permutation_iterator<hvec<float>::iterator,hvec<uint>::iterator>(tX.begin(), tK.end()),
	       Y.begin());
 
  for(uint d=0;d<nd;d++){
    cudaSetDevice(d);
    dpA[d]=thrust::device_malloc<float>(n);thrust::copy(A.begin(),A.end(),dpA[d]);
    dpY[d]=thrust::device_malloc<float>(n);thrust::copy(Y.begin(),Y.end(),dpY[d]);
    dpG[d]=thrust::device_malloc<uint>(n); thrust::copy(G.begin(),G.end(),dpG[d]);
  }
    
  uint div = (n+nd-1)/nd;
  for(uint d=0;d<nd;d++){
      q[d] = *std::lower_bound(S.begin(),S.end(),d*div);
      //printf("%d:inf(S>%d)=%d\n",d,d*div,q[d]);
  }
  q[nd]=S.back();
  //std::for_each(q,q+nd+1,printf_functor());printf("\n");
  cudaSetDevice(0);
  event_pair timer;
  start_timer(&timer);
  #pragma omp parallel for
  for(uint d=0;d<nd;d++){
    cudaSetDevice(d);
    for(int i=0;i<N;i++){
      thrust::transform(dpA[d]+q[d],
			dpA[d]+q[d+1],
			dpY[d]+q[d],
			dpA[d]+q[d],
			thrust::multiplies<float>());
      thrust::inclusive_scan_by_key(dpG[d]+q[d],
				    dpG[d]+q[d+1],
				    dpA[d]+q[d],
				    dpA[d]+q[d]);
    }  
  }
  cudaSetDevice(0);
  check_launch("Thrust3");
  float t=stop_timer(&timer,"Thrust3");
  
  for(uint d=0;d<nd;d++){
    cudaSetDevice(d);
    thrust::copy(dpA[d]+q[d],dpA[d]+q[d+1],&A[q[d]]);
    thrust::device_free(dpA[d]);
    thrust::device_free(dpY[d]);
    thrust::device_free(dpG[d]);
  }
  return t;
}
