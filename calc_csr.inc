
__global__ void kernel_csr(float* A,uint* K, uint* S,float* X,uint n,uint p,uint q,uint N){
  int  i= (blockIdx.y*gridDim.x+ blockIdx.x) * blockDim.x + threadIdx.x;
  if(i<p-1)
    for(int k=0;k<N;k++){
      float sum=0;
      for(int j=S[i];j<S[i+1];j++){
	sum+=A[j]*X[K[j]];
	A[j]=sum;
      }
    }
}

		    
float calc_csr(std::vector<float> &A,std::vector<uint> &K,std::vector<uint> &S,std::vector<float> &X){
  tvec<float> tX(X);
  tvec<float> tA(A);
  tvec<uint> tK(K);
  tvec<uint> tS(S);
  event_pair timer;
  start_timer(&timer);
  uint nt=32;
  uint nx=(p-1)/nt+1;
  dim3 nb(nx,1,1);
  kernel_csr<<<nb,nt>>>
    (thrust::raw_pointer_cast(tA.data()),
     thrust::raw_pointer_cast(tK.data()),
     thrust::raw_pointer_cast(tS.data()),
     thrust::raw_pointer_cast(tX.data()),
     n,p,q,N);
  check_launch("CSR");
  float t=stop_timer(&timer,"CSR");
  thrust::copy(tA.begin(),tA.end(),A.begin());
  return t;
}

float calc_csr_multi(std::vector<float> &A,std::vector<uint> &K,std::vector<uint> &S,std::vector<float> &X){
  int nd;
  cudaGetDeviceCount(&nd);
  omp_set_num_threads(nd);
  thrust::device_ptr<float> dpA[nd];
  thrust::device_ptr<float> dpX[nd];
  thrust::device_ptr<uint>  dpK[nd];
  thrust::device_ptr<uint>  dpS[nd];
  uint div = (p+nd-1)/nd;
  uint dp[nd];
  for(uint d=0;d<nd;d++){
    cudaSetDevice(d);
    dp[d] = std::min((d+1)*div,p-1)-d*div;
    dpA[d]=thrust::device_malloc<float>(A.size());thrust::copy(A.begin(),A.end(),dpA[d]);
    dpX[d]=thrust::device_malloc<float>(X.size());thrust::copy(X.begin(),X.end(),dpX[d]);
    dpK[d]=thrust::device_malloc<uint>(K.size());thrust::copy(K.begin(),K.end(),dpK[d]);
    dpS[d]=thrust::device_malloc<uint>(dp[d]);thrust::copy(S.begin()+d*div,S.begin()+d*div+dp[d],dpS[d]);
  }
  cudaSetDevice(0);
  event_pair timer;
  start_timer(&timer);
  //#pragma omp parallel for
  for(uint d=0;d<nd;d++){
    cudaSetDevice(d);
    uint nt=32;
    dim3 nb((dp[d]-1)/nt+1,1,1);
    kernel_csr<<<nb,nt>>>
      (thrust::raw_pointer_cast(dpA[d]),
       thrust::raw_pointer_cast(dpK[d]),
       thrust::raw_pointer_cast(dpS[d]),
       thrust::raw_pointer_cast(dpX[d]),
       n,dp[d],q,N);
  }
  cudaSetDevice(0);
  check_launch("CSR Multi");
  float t=stop_timer(&timer,"CSR Multi");
  for(uint d=0;d<nd;d++){
    cudaSetDevice(d);
    //printf("S0:%d S1:%d\n",S[d*div],S[d*div+dp[d]]);
    thrust::copy(dpA[d]+S[d*div],dpA[d]+S[d*div+dp[d]],&A[S[d*div]]);
    thrust::device_free(dpA[d]);
    thrust::device_free(dpX[d]);
    thrust::device_free(dpK[d]);
    thrust::device_free(dpS[d]);
  }
  return t;
}

__global__ void kernel_blockcsr(float* A,uint* K, uint* S,float* X,uint n,uint p,uint q,uint N){
  uint is= (blockIdx.y*gridDim.x+ blockIdx.x);
  if(is<p-1){
    for(int it=0;it<N;it++){
      for (int ia=S[is]+threadIdx.x;ia<S[is+1];ia+=blockDim.x)
	A[ia] = A[ia]*X[K[ia]];
      __syncthreads();
      for(int shift=1;shift<S[is+1]-S[is];shift*=2){
	for (int ia=S[is]+threadIdx.x;ia<S[is+1];ia+=blockDim.x)
	  if(ia-shift>=S[is])A[ia] = A[ia]+A[ia-shift];
	__syncthreads();
      }
    }
  }
}
float calc_blockcsr(std::vector<float> &A,std::vector<uint> &K,std::vector<uint> &S,std::vector<float> &X){
  tvec<float> tX(X);
  tvec<float> tA(A);
  tvec<uint> tK(K);
  tvec<uint> tS(S);
  event_pair timer;
  start_timer(&timer);
  uint nt=32;
  dim3 nb(min(p,0xffff),p/0xffff+1,1);
  kernel_blockcsr<<<nb,nt>>>
    (thrust::raw_pointer_cast(tA.data()),
     thrust::raw_pointer_cast(tK.data()),
     thrust::raw_pointer_cast(tS.data()),
     thrust::raw_pointer_cast(tX.data()),
     n,p,q,N);
  check_launch("Block CSR");
  float t=stop_timer(&timer,"Block CSR");
  thrust::copy(tA.begin(),tA.end(),A.begin());
  return t;
}


__global__ void kernel_csr_ordered(float* A,uint* K, uint* S,float* X,uint* O,uint n,uint p,uint q,uint N){
  int  i= (blockIdx.y*gridDim.x+ blockIdx.x) * blockDim.x + threadIdx.x;
  if(i<p-1){
    i=O[i];
    for(int k=0;k<N;k++){
      float sum=0;
      for(int j=S[i];j<S[i+1];j++){
	sum+=A[j]*X[K[j]];
	A[j]=sum;
      }
    }
  }
}
float calc_csr_ordered(std::vector<float> &A,std::vector<uint> &K,std::vector<uint> &S,std::vector<float> &X){
  tvec<float> tX(X);
  tvec<float> tA(A);
  tvec<uint> tK(K);
  tvec<uint> tS(S);
  tvec<uint> tL(p-1),tO(p-1);
  
  thrust::adjacent_difference(tS.begin(),tS.end(),tL.begin());
  thrust::sequence(tO.begin(),tO.end());
  thrust::sort_by_key(tL.begin(),tL.end(),tO.begin(),thrust::greater<float>());
  
  event_pair timer;
  start_timer(&timer);
  uint nt=32;
  uint nx=(p-1)/nt+1;
  dim3 nb(nx,1,1);
  //thrust::for_each(tO.begin(),tO.end(),printf_functor());
  kernel_csr_ordered<<<nb,nt>>>
    (thrust::raw_pointer_cast(tA.data()),
     thrust::raw_pointer_cast(tK.data()),
     thrust::raw_pointer_cast(tS.data()),
     thrust::raw_pointer_cast(tX.data()),
     thrust::raw_pointer_cast(tO.data()),
     n,p,q,N);
  check_launch("CSR_Ordered");
  float t=stop_timer(&timer,"CSR_Ordered");
  thrust::copy(tA.begin(),tA.end(),A.begin());
  return t;
}

texture<float, 1, cudaReadModeElementType> t_X;
__global__ void kernel_csr_tex(float* A,uint* K, uint* S,float* X,uint n,uint p,uint q,uint N){
  int  i= (blockIdx.y*gridDim.x+ blockIdx.x) * blockDim.x + threadIdx.x;
  if(i<p)
    for(int k=0;k<N;k++){
      float sum=0;
      for(int j=S[i];j<S[i+1];j++){
	sum+=A[j]*tex1D(t_X,K[j]);
	A[j]=sum;
      }
    }
}
float calc_csr_tex(std::vector<float> &A,std::vector<uint> &K,std::vector<uint> &S,std::vector<float> &X){
  tvec<float> tX(X);
  tvec<float> tA(A);
  tvec<uint> tK(K);
  tvec<uint> tS(S);
  //create cuda array
  cudaArray* d_ary_X;
  cudaChannelFormatDesc channelDesc = cudaCreateChannelDesc(32, 0, 0, 0, cudaChannelFormatKindFloat); 
  cudaMallocArray( &d_ary_X, &channelDesc, X.size(), 1 ); 
  cudaMemcpyToArray( d_ary_X, 0, 0,thrust::raw_pointer_cast(&tX[0]) , X.size() * sizeof (float), cudaMemcpyDeviceToDevice); 
  // Bind the array to the texture 
  cudaBindTextureToArray( t_X, d_ary_X, channelDesc);
  

  event_pair timer;
  start_timer(&timer);
  uint nthreads=32;
  dim3 nblocks((p-1)/nthreads,1,1);
  kernel_csr_tex<<<nblocks,nthreads>>>
    (thrust::raw_pointer_cast(&tA[0]),
     thrust::raw_pointer_cast(&tK[0]),
     thrust::raw_pointer_cast(&tS[0]),
     thrust::raw_pointer_cast(&tX[0]),
     n,p,q,N);
  check_launch("Threadrow");
  float t=stop_timer(&timer,"Threadrow");
  thrust::copy(tA.begin(),tA.end(),A.begin());
  return t;
}

__global__ void kernel_aligned(float* A,float* Y, uint* R, uint n,uint N){
  int  i= (blockIdx.y*gridDim.x+ blockIdx.x) * blockDim.x + threadIdx.x;
  if (i<n){
    for(int it=0;it<N;it++){
      A[i]=A[i]*Y[i];
      for(int s=1;i-s>0 && R[i-s]==R[i];s*=2){
	A[i]+=A[i-s];
	//atomicAdd(&A[i],A[i-s]);
      }
    }
  }
}
float calc_aligned_multi(std::vector<float> &A,std::vector<uint> &K,std::vector<uint> &S,std::vector<float> &X){
  int nd;
  cudaGetDeviceCount(&nd);
  omp_set_num_threads(nd);
  uint q[nd+1];
  hvec<float>  Y(n);
  hvec<uint>   G(n,0);
  hvec<uint>  tS(S);
  hvec<float> tX(X);
  hvec<uint>  tK(K);
  thrust::device_ptr<float> dpA[nd];
  thrust::device_ptr<float> dpY[nd];
  thrust::device_ptr<uint>  dpG[nd];
  
  thrust::fill(thrust::permutation_iterator<hvec<uint>::iterator,hvec<uint>::iterator>(G.begin(), tS.begin()),
	       thrust::permutation_iterator<hvec<uint>::iterator,hvec<uint>::iterator>(G.begin(), tS.end()),
	       1);
  
  thrust::inclusive_scan(G.begin(),G.end(),G.begin());
    
  thrust::copy(thrust::permutation_iterator<hvec<float>::iterator,hvec<uint>::iterator>(tX.begin(), tK.begin()),
	       thrust::permutation_iterator<hvec<float>::iterator,hvec<uint>::iterator>(tX.begin(), tK.end()),
	       Y.begin());
 
  for(uint d=0;d<nd;d++){
    cudaSetDevice(d);
    dpA[d]=thrust::device_malloc<float>(n);thrust::copy(A.begin(),A.end(),dpA[d]);
    dpY[d]=thrust::device_malloc<float>(n);thrust::copy(Y.begin(),Y.end(),dpY[d]);
    dpG[d]=thrust::device_malloc<uint>(n); thrust::copy(G.begin(),G.end(),dpG[d]);
  }
    
  uint div = (n+nd-1)/nd;
  for(uint d=0;d<nd;d++){
      q[d] = *std::lower_bound(S.begin(),S.end(),d*div);
      //printf("%d:inf(S>%d)=%d\n",d,d*div,q[d]);
  }
  q[nd]=S.back();
  //std::for_each(q,q+nd+1,printf_functor());printf("\n");
  cudaSetDevice(0);
  event_pair timer;
  start_timer(&timer);
  #pragma omp parallel for
  for(uint d=0;d<nd;d++){
    cudaSetDevice(d);
    uint dn = q[d+1]-q[d];
    uint nthreads=32;
    dim3 nblocks((dn-1)/nthreads,1,1);
    kernel_aligned<<<nblocks,nthreads>>>
      (thrust::raw_pointer_cast(dpA[d]+q[d]),
       thrust::raw_pointer_cast(dpY[d]+q[d]),
       thrust::raw_pointer_cast(dpG[d]+q[d]),
       dn,N);  
  }
  cudaSetDevice(0);
  check_launch("Aligned Multi");
  float t=stop_timer(&timer,"Aligned Multi");
  
  for(uint d=0;d<nd;d++){
    cudaSetDevice(d);
    thrust::copy(dpA[d]+q[d],dpA[d]+q[d+1],&A[q[d]]);
    thrust::device_free(dpA[d]);
    thrust::device_free(dpY[d]);
    thrust::device_free(dpG[d]);
  }
  return t;
}

