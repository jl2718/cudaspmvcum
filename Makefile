all : final

final : final.cu calc_serial.inc calc_thrust.inc calc_csr.inc
	nvcc -O3 -o fp final.cu -lgomp -arch=sm_20 -Xcompiler -fopenmp
clean :
	rm fp